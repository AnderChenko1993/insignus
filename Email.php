<?php 
	require_once("components/PHPMailer/CPHPMailer.php");

	class Email {

		public function enviar($data) {
			$mail = new CPHPMailer;

			// Charset para evitar erros de caracteres
			$mail->CharSet = 'UTF-8';
			 
			// Dados de quem está enviando o email
			$mail->From = $data['email'];
			$mail->FromName = $data['nome'];
			 
			// Setando o conteudo
			$mail->IsHTML(true);
			$mail->Subject = "CONTATO - INSIGNE";
			$mail->Body = $data['mensagem'];

			// Validando a autenticação
			$mail->IsSMTP();
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "tls";  
			$mail->Host     = "smtp.gmail.com";
			$mail->Port     = 587;
			$mail->Username = "contatophpteste@gmail.com";
			$mail->Password = "contato@phpteste";

			// Setando o endereço de recebimento
			$mail->AddAddress("contato@insignus.com.br", "Anderson Costa - teste");

			$enviado = false;
			
			// Enviando o e-mail
			if ($mail->Send()) {
				$enviado = true;
			}
			else {
				$error = 'Mail error: '.$mail->ErrorInfo; 
				print($error);
			}

			return $enviado;

		}

	}

?>